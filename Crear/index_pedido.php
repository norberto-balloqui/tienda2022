<?php
    include("../Modulo/ProductoBackend.php");
    require_once('conexionpdo.php');

    
    $sql = "SELECT ins_id, ins_nombre FROM INSTITUCION ORDER BY ins_nombre ASC";
    $sql=$pdo->prepare($sql);
    $sql->execute();
    
    $resultado=$sql->fetchAll();
   
    $sql2 = "SELECT tall_id, tall_nombre FROM TALLA ORDER BY tall_nombre ASC";
    $sql2=$pdo->prepare($sql2);
    $sql2->execute();
    $resultado2=$sql2->fetchAll();
    
    header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de pedidos</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> 
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- Datepicker -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.bootstrap4.min.css">
    <!-- Style CSS-->
    <link rel="stylesheet" href="css/style.css">
    
</head>
<body>
    <div class="d-flex" id="wrapper">
        <!-- Sidebar izquierdo-->
        <div class="bg-dark text-white" id="sidebar-wrapper">
            <div class="sidebar-heading  bg-dark">Menu</div>
            <div class="list-group list-group-flush">
                <a href="Crear/Volver/volverpedido.php" class="list-group-item list-group-item-action bg-dark text-white"><i class="fas fa-home"></i> Inicio</a>
                <a href="#" class="list-group-item list-group-item-action bg-dark text-white"><i class="fas fa-users"></i> Administrador</a>


            </div>
        </div>

        <div id="page-content-wrapper">
            <!-- Navbar arriba-->
            <nav class="navbar navbar-expand-lg bg-dark">
                <button class="btn btn-secondary" id="menu-toggle"><i class="fas fa-bars"></i></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                    
                    <!-- accines arriba
						 <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Acciones
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action1</a>
                        <a class="dropdown-item" href="#">Action2</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">accion3</a>
                        </div>
                    </li>
                    -->
                    
                </div>
            </nav>

			<div class="container" style="margin-top: 80px;">
        <h2>Pedidos</h2>
        <hr>
        <div class="row">
		<div class="col-3">            
                        <button class="btn btn-primary form-control" data-toggle="modal" data-target="#crearNuevoEmpleado">
                            Crear un producto nuevo
                        </button>
                    </div>
          
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class  ="thead-dark">
                 <tr>
                    <th>N°</th>
                     
                    <th></th>
                    
                    <th>Nombre producto</th>
                    <th>Precio</th>
                    <th>Institución</th>
                    <th>Talla</th>
                     		

                </tr>
               </thead>
                <?php
                $contador = 1;
                if($ConsultaPedido): foreach($ConsultaPedido as $row): ?>

                <tr>
                    <td><?= $contador ?></td>     
                    
                    
                    <td style="visibility:hidden;"><?= $row['pro_id']?></td>
                    
                    <td><?= $row['pro_nombre']?></td>
                    <td><?= $row['pro_precio']?></td>
                    <td><?= $row['ins_nombre']?></td>
                    <td><?= $row['tall_nombre']?></td>
                   
					    
                    <td>
                   <!--norberto boton modificar reunion-->  
                   <button type="button" class="btn btn-primary editar"
                                        
                                        data-ped_id='<?= $row['ped_id']?>'
                                        data-ped_fecha_reg='<?= $row['ped_fecha_reg']?>'
                                        data-ped_fecha_des='<?= $row['ped_fecha_des']?>'
                                        data-ven_precio_total='<?= $row['ven_precio_total']?>'
                                        data-ven_abono='<?= $row['ven_abono']?>'
                                        data-ped_comentario='<?= $row['ped_comentario']?>'
                                        data-cli_id='<?= $row['cli_id']?>'
										data-cli_id='<?= $row['est_id']?>'
                                        data-toggle="modal" data-target="#EditarReunion">
                                        <i class="fas fa-edit"></i>
                                </button>
               
                    
                    </td>
                    
                    

                    <?php $contador++; ?>
                </tr>

                

                <?php endforeach; endif?>
                
            </table>
        </div>
    </div>
                 
	
    <!-- CrearNuevo Empleado Modal -->
    <div class="modal fade" id="crearNuevoEmpleado" tabindex="-1">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Crear producto nuevo</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form action="Crear/CrearProductor.php" method="POST">
                    <div class="row">
                        <div class="form-group col-5">
                            <label>Nombre producto</label>
                            <input class="form-control" type="text" name="pro_nombre" id="pro_nombre" placeholder="Se puede ingresar solo números y letras"  minlength="1" maxlength="35" required="" pattern="[a-z A-ZñÑáéíóúÁÉÍÓÚ0-9]{1,35}" >
                        </div>
						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;

                        <div class="form-group col-5">
                            <label>Precio producto</label>
                            <input class="form-control" type="int" name="pro_precio" id="pro_precio" placeholder="Se puede ingresar solo números" minlength="1" maxlength="8" pattern="[0-9.]{1,8}" required >
        
                        </div>
                    </div>
                    <div class="form-group row">
           <label for="validationTooltip04" class="col-lg-2 col-form-label form-control-label"></label>
        <div class="col-lg-8"> Seleccionar institución
             <select required aria-required="true"
              class="form-control" name="ins_id"  id="validationTooltip04" required>
             
             <option selected disabled value="">Seleccionar</option>

            <?php for($i=0; $i<count($resultado);$i++) { ?>
                    
                <option value="<?php echo $resultado[$i]['ins_id']; ?>"><?php echo utf8_encode($resultado[$i]['ins_nombre']); ?></option>


                <?php } 
                
                ?>        
             </select>
             <div class="invalid-tooltip">
      Seleccionar una institución valida
             </div>
        </div>
    </div>




    <div class="form-group row">
        <label for="validationTooltip04" class="col-lg-2 col-form-label form-control-label"></label>

        <div class="col-lg-8"> Seleccionar talla
            <select required aria-required="true"
             class="form-control" name="tall_id" id="validationTooltip04" required>
            
             <option selected disabled value="">Seleccionar</option>

            <?php for($i=0; $i<count($resultado2);$i++) { ?>
                    <option value="<?php echo $resultado2[$i]['tall_id']; ?>"><?php echo utf8_encode($resultado2[$i]['tall_nombre']); ?></option>
               
                    <?php } ?>
         
           </select>
           <div class="invalid-tooltip">
      Seleccionar una talla valida
             </div>

        </div>
    </div>
 
                    <div class="offset-10">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                  </form>
            </div>
        </div>
        </div>
    </div>
	
    <!--Inicio modal de respuesta  -->
    <div class="modal fade" id="Respuesta" role="dialog">
          <div class="modal-dialog" >
            <div class="modal-content" style="margin-bottom:0;">
              <div id="alerta" class="modal-body" role="alert" style="position: relative; margin: 8px;">
                <i style="position: absolute; left: 85%; margin-bottom: 10%;" class="fas fa-info-circle"></i> 
                <div id="mensajeServidor" class="form-group" style="padding-top: 10px; margin: 0;">
                  
                </div> 
              </div>
            </div>
          </div>
        </div>
        <!-- Fin modal de respuesta -->

    <!-- jQuery -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <!-- Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <!-- Datepicker -->
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    
	<script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('#datepicker2').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
	
	
	<!-- Datatables JS -->
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.bootstrap4.min.js"></script>
    <!-- Index JS -->                   
    <script src="js/index.js"></script>
    <!--libreria sweet alert--->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

</body>
</html>
