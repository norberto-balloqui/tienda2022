
<?php
include("Modulo/PedidoBackend.php");

require_once('conexionpdo.php');




$sql = "SELECT cli_id, cli_nombre FROM CLIENTE ORDER BY cli_nombre ASC";
    $sql=$pdo->prepare($sql);
    $sql->execute();
    $resultado=$sql->fetchAll();

$sql2 = "SELECT est_id, est_nombre FROM ESTADO ORDER BY est_nombre ASC";
    $sql2=$pdo->prepare($sql2);
    $sql2->execute();
    $resultado2=$sql2->fetchAll();


?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de pedidos</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous"> 
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <!-- Datepicker -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.bootstrap4.min.css">
    <!-- Style CSS-->
    <link rel="stylesheet" href="css/style.css">
    
</head>
<body>
    <div class="d-flex" id="wrapper">
        <!-- Sidebar izquierdo-->
        <div class="bg-dark text-white" id="sidebar-wrapper">
            <div class="sidebar-heading  bg-dark">Menu</div>
            <div class="list-group list-group-flush">
                <a href="index.php" class="list-group-item list-group-item-action bg-dark text-white"><i class="fas fa-home"></i> Inicio</a>
                <a href="index2.php" class="list-group-item list-group-item-action bg-dark text-white"><i class="fas fa-users"></i> Administrador</a>


            </div>
        </div>

        <div id="page-content-wrapper">
            <!-- Navbar arriba-->
            <nav class="navbar navbar-expand-lg bg-dark">
                <button class="btn btn-secondary" id="menu-toggle"><i class="fas fa-bars"></i></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                    
                    <!-- accines arriba
						 <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Acciones
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action1</a>
                        <a class="dropdown-item" href="#">Action2</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">accion3</a>
                        </div>
                    </li>
                    -->
                    
                </div>
            </nav>

			<div class="container" style="margin-top: 80px;">
        <h2>Pedidos</h2>
        <hr>
        <div class="row">
		<div class="col-3">            
                        <button class="btn btn-primary form-control" data-toggle="modal" data-target="#crearNuevoEmpleado">
                            Crear un pedido nuevo
                        </button>
                    </div>
          
        </div>
        <br>
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class  ="thead-dark">
                 <tr>
                    <th>N°</th>
                     
                    <th></th>
                    
                    <th>Fecha tomada</th>
                    <th>Fecha termino</th>
                    <th>Venta</th>
                    <th>Abono</th>
                    <th>Detalle</th>
                    <th>Cliente</th>
					<th>Estado</th> 
                    <th>Acciones</th> 		

                </tr>
               </thead>
                <?php
                $contador = 1;
                if($ConsultaPedido): foreach($ConsultaPedido as $row): ?>

                <tr>
                    <td><?= $contador ?></td>     
                    
                    
                    <td style="visibility:hidden;"><?= $row['ped_id']?></td>
                    
                    <td><?= $row['ped_fecha_reg']?></td>
                    <td><?= $row['ped_fecha_des']?></td>
                    <td><?= $row['ven_precio_total']?></td>
                    <td><?= $row['ven_abono']?></td>
                    <td><?= $row['ped_comentario']?></td>
                    <td><?= $row['cli_nombre']?></td>
					<td><?= $row['est_nombre']?></td>
					    
                    <td>
<<<<<<< HEAD
                   
=======
                   <!--norberto boton modificar reunion-->  
                    <button type="button" class="btn btn-primary editar"
                                        
                                        data-ped_id='<?= $row['ped_id']?>'
                                        data-ped_fecha_reg='<?= $row['ped_fecha_reg']?>'
                                        data-ped_fecha_des='<?= $row['ped_fecha_des']?>'
                                        data-ven_precio_total='<?= $row['ven_precio_total']?>'
                                        data-ven_abono='<?= $row['ven_abono']?>'
                                        data-ped_comentario='<?= $row['ped_comentario']?>'
                                        data-cli_id='<?= $row['cli_id']?>'
										data-cli_id='<?= $row['est_id']?>'
                                        data-toggle="modal" data-target="#EditarReunion">
                                        <i class="fas fa-edit"></i>
                                </button>
>>>>>>> 8dada3569563e411e6ebebbf99c88f7eefac3e1e
               
                    
                    </td>
                    
                    

                    <?php $contador++; ?>
                </tr>

                

                <?php endforeach; endif?>
                
            </table>
        </div>
    </div>
                 
	
    <!-- CrearNuevo Empleado Modal -->
    <div class="modal fade" id="crearNuevoEmpleado" tabindex="-1">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Crear pedido nuevo</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form action="Crear/CrearPedido.php" method="POST">
                    <div class="row">
                        <div class="form-group col-5">
                            <label>Fecha registrada</label>
                            <input class="form-control" type="date" name="ped_fecha_reg" id="ped_fecha_reg"  required min="2022-09-01" max="2030-01-01" >
                        </div>
						&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;

                        <div class="form-group col-5">
                            <label>Fecha entrega</label>
                            <input class="form-control" type="date" name="ped_fecha_des" id="ped_fecha_des" 
            required min="2022-09-01" max="2030-01-01" >
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label>Precio de venta</label>
                            <input class="form-control" type="int" name="ven_precio_total" id="ven_precio_total" placeholder="Ingresar solo números" minlength="1" maxlength="8" pattern="[0-9.]{1,8}" required >
                        </div>
                            <div class="form-group col-6">
                            <label>Monto de abono</label>
							<input class="form-control" type="int" name="ven_abono" id="ven_abono" placeholder="Ingresar solo números" minlength="1" maxlength="8" pattern="[0-9.]{1,8}" required >
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-9">
                            <label>Comentario</label>
                            <input class="form-control" type="text" placeholder="maximo 47 caracteres" style="width : 400px; heigth : 400px" name="ped_comentario" id="ped_comentario" maxlength="47" pattern="[a-z A-Z0-9]{0,47}">
                        </div>

						<div class="form-group row">
     <label class="col-lg-2 col-form-label form-control-label"></label>
        <div class="col-lg-8"> Seleccionar cliente
             <select class="form-control" name="cli_id" id="validationTooltip04" required>
             <option selected disabled value="">Seleccionar</option>

               <?php for($i=0; $i<count($resultado);$i++) { ?>
                    <option value="<?php echo $resultado[$i]['cli_id']; ?>"><?php echo utf8_encode($resultado[$i]['cli_nombre']); ?></option>
                <?php } ?>        
             </select>
             <div class="invalid-tooltip">
      Seleccionar un cliente valido
             </div>
        </div>
    </div>

	&nbsp;
    <div class="form-group row">
        <label for="validationTooltip04" class="col-lg-2 col-form-label form-control-label"></label>
        <div class="col-lg-8"> Seleccionar estado
            <select class="form-control" name="est_id" id="validationTooltip04" required>
            <option selected disabled value="">Seleccionar</option>

               <?php for($i=0; $i<count($resultado2);$i++) { ?>
                    <option value="<?php echo $resultado2[$i]['est_id']; ?>"><?php echo utf8_encode($resultado2[$i]['est_nombre']); ?></option>
                <?php } ?>

           </select>
           <div class="invalid-tooltip">
      Seleccionar una estado valido
             </div>
        </div>
    </div>
	&nbsp; &nbsp;
                    <div class="offset-10">
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                  </form>
            </div>
        </div>
        </div>
    </div>
	
<<<<<<< HEAD
  
=======
    <!-- Editar Empleado Modal -->
    <div class="modal fade" id="editarEmpleado" tabindex="-1">
        <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Editar Empleado</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="form-group col-3">
                            <label>Codigo</label>
							<input class="form-control" type="date" name="pedidofecha" id="datepicker2"  required min="2022-09-01" max="2030-01-01" >
                        </div>
                        <div class="form-group col-9">
                            <label>Nombre</label>
							<input class="form-control" type="date" name="retirarfecha" id="datepiecker2" 
            required min="2022-09-01" max="2030-01-01" >
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label>Lugar de nacimiento</label>
                            <input class="form-control" type="int" name="ven_precio_totalModal" id="ven_precio_totalModal" placeholder="Ingresar solo números" minlength="1" maxlength="8" pattern="[0-9.]{1,8}" required >
                        </div>
                            <div class="form-group col-6">
                            <label>Fecha de nacimiento</label>
							<input class="form-control" type="int" name="ven_abonoModal" id="ven_abonoModal" placeholder="Ingresar solo números" minlength="1" maxlength="8" pattern="[0-9.]{1,8}" required >
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label>Telefono</label>
                            <input class="form-control" type="text" placeholder="maximo 47 caracteres" style="width : 400px; heigth : 400px" name="ped_comentarioModal" id="ped_comentarioModal" maxlength="47" pattern="[a-z A-Z0-9]{0,47}">
                        </div>
          

					<div class="form-group row">
     <label class="col-lg-2 col-form-label form-control-label"></label>
        <div class="col-lg-8"> Seleccionar cliente
             <select class="form-control" name="cli_idModal" id="validationTooltip04" required>
             <option selected disabled value="">Seleccionar</option>

               <?php for($i=0; $i<count($resultado);$i++) { ?>
                    <option value="<?php echo $resultado[$i]['cli_idModal']; ?>"><?php echo utf8_encode($resultado[$i]['cli_nombre']); ?></option>
                <?php } ?>        
             </select>
             <div class="invalid-tooltip">
      Seleccionar un cliente valido
             </div>
        </div>
    </div>

	&nbsp;
    <div class="form-group row">
        <label for="validationTooltip04" class="col-lg-2 col-form-label form-control-label"></label>
        <div class="col-lg-8"> Seleccionar estado
            <select class="form-control" name="est_idModal" id="validationTooltip04" required>
            <option selected disabled value="">Seleccionar</option>

               <?php for($i=0; $i<count($resultado2);$i++) { ?>
                    <option value="<?php echo $resultado2[$i]['est_idModal']; ?>"><?php echo utf8_encode($resultado2[$i]['est_nombre']); ?></option>
                <?php } ?>

           </select>
           <div class="invalid-tooltip">
      Seleccionar una estado valido
             </div>
        </div>
    </div>
	&nbsp; &nbsp;




                    
                    <div class="offset-10">
                        <button type="button" class="btn btn-primary" id='guardarEmpleadosEditar'>Enviar</button>
>>>>>>> 8dada3569563e411e6ebebbf99c88f7eefac3e1e
                    </div>
                  </form>
            </div>
        </div>
        </div>
    </div>



    <!--Inicio modal de respuesta  -->
    <div class="modal fade" id="Respuesta" role="dialog">
          <div class="modal-dialog" >
            <div class="modal-content" style="margin-bottom:0;">
              <div id="alerta" class="modal-body" role="alert" style="position: relative; margin: 8px;">
                <i style="position: absolute; left: 85%; margin-bottom: 10%;" class="fas fa-info-circle"></i> 
                <div id="mensajeServidor" class="form-group" style="padding-top: 10px; margin: 0;">
                  
                </div> 
              </div>
            </div>
          </div>
        </div>
        <!-- Fin modal de respuesta -->

    <!-- jQuery -->
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <!-- Bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <!-- Datepicker -->
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    
	<script>
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });

        $('#datepicker2').datepicker({
            uiLibrary: 'bootstrap4'
        });
    </script>
	
	
	<!-- Datatables JS -->
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.bootstrap4.min.js"></script>
    <!-- Index JS -->                   
    <script src="js/index.js"></script>
    <!--libreria sweet alert--->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

</body>
</html>
